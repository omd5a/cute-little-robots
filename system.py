import sys
import system
import analogio
import digitalio
import board
import pulseio
import time
import machine

# limit motor duty cycle
# could go as high as 4.8V/9V
# but, to help preserve battery capacity, we'll go lower than that
max_duty = (2.4/9)

class Robot:

	def __enter__(self):
		self.led_pwm = pulseio.PWMOut(board.GPIO2, frequency=1000)
		self.floor_sensor = analogio.AnalogIn(board.ADC)
		self.standby = digitalio.DigitalInOut(board.GPIO0)
		self.standby.direction = digitalio.Direction.OUTPUT
		self.pwma = pulseio.PWMOut(board.GPIO15, frequency=1000)
		self.ain1 = digitalio.DigitalInOut(board.GPIO5)
		self.ain1.direction = digitalio.Direction.OUTPUT
		self.ain2 = digitalio.DigitalInOut(board.GPIO4)
		self.ain2.direction = digitalio.Direction.OUTPUT
		self.pwmb = pulseio.PWMOut(board.GPIO12, frequency=1000)
		self.bin1 = digitalio.DigitalInOut(board.GPIO13)
		self.bin1.direction = digitalio.Direction.OUTPUT
		self.bin2 = digitalio.DigitalInOut(board.GPIO14)
		self.bin2.direction = digitalio.Direction.OUTPUT
		self.bin1.value = False
		self.bin2.value = False
		self.standby.value = True
		self.travel = 1 # -1 == reverse; 0 == stop; 1 == forward
		self.direction = 0 # -1 == left; 0 == straight; 1 == right
		self.w_over_r = 0 # turn factor
		self.s = 0
		return self
   
	def __exit__(self, type, value, traceback):
		self.led_pwm.deinit()
		self.floor_sensor.deinit()
		self.standby.deinit()
		self.pwma.deinit()
		self.ain1.deinit()
		self.ain2.deinit()
		self.pwmb.deinit()
		self.bin1.deinit()
		self.bin2.deinit()

	def update_led(self):
		if self.led_state:
			value = max(0, self.led_brightness)
			value = min(100, self.led_brightness)
			value = 100 - value
			self.led_pwm.duty_cycle = (int) (value * 65535 / 100)
		else:
			self.led_pwm.duty_cycle = 65535
		
	def set_led_on(self):
		self.led_state = True
		self.update_led()

	def set_led_off(self):
		self.led_state = False
		self.update_led()
			
	def set_led_brightness(self, value):
		self.led_brightness = value
		self.update_led()

	def glow(self, value):
		value = max(0, value)
		value = min(100, value)
		value = 100 - value
		self.led_pwm.duty_cycle = (int) (value * 65535 / 100)

	def flash(self, value):
		value = max(0.1, value)
		value = min(100, value)
		self.led_pwm.frequency = value
		self.led_pwm.duty_cycle = 32768 # 50%

	# high if the floor is bright
	# low if the floor is dark
	def floor(self):
		value = self.floor_sensor.value
		value = int(float(value) * 3.3 / 65535 * 114 - 22) 
		value = min(value, 100)
		value = max(value, 0)
		value = 100 - value
		return float(value)

	def a_speed(self, value):
		value = max(0, value)
		value = min(100, value)
		self.pwma.duty_cycle = (int) (float(value) * max_duty * 65535 / 100)

	def a_forward(self):
		self.ain1.value = True
		self.ain2.value = False

	def a_reverse(self):
		self.ain1.value = False
		self.ain2.value = True

	def a_stop(self):
		self.ain1.value = False
		self.ain2.value = False

	def b_speed(self, value):
		value = max(0, value)
		value = min(100, value)
		self.pwmb.duty_cycle = (int) (float(value) * max_duty * 65535 / 100)

	def b_forward(self):
		self.bin1.value = False
		self.bin2.value = True

	def b_reverse(self):
		self.bin1.value = True
		self.bin2.value = False

	def b_stop(self):
		self.bin1.value = False
		self.bin2.value = False

	def set_motors(self):
		if self.travel == 0:
			self.a_stop()
			self.b_stop()
		elif self.direction == 0 or self.w_over_r == 0:
			if self.travel == 1:
				self.a_forward()
				self.b_forward()
			else:
				self.a_reverse()
				self.b_reverse()
			self.a_speed(self.s)
			self.b_speed(self.s)
		else:
			r = float(self.w_over_r) / 100
			if self.direction == -1:
				a = self.s * (1 - 2 * r)
				b = self.s * (1 + 2 * r)
			else:
				a = self.s * (1 + 2 * r)
				b = self.s * (1 - 2 * r)
			a = max(-100, min(100, a))
			b = max(-100, min(100, b))
			if self.travel == -1:
				a = -a
				b = -b
			if a >= 0:
				self.a_forward()
			else:
				self.a_reverse()
			self.a_speed(abs(a))
			if b >= 0:
				self.b_forward()
			else:
				self.b_reverse()
			self.b_speed(abs(b))

	def forward(self):
		self.travel = 1
		self.set_motors()

	def reverse(self):
		self.travel = -1
		self.set_motors()

	def stop(self):
		self.travel = 0
		self.set_motors()

	def speed(self, value):
		value = max(0, value)
		value = min(100, value)
		self.s = value
		self.set_motors()

	def left(self, value):
		value = max(0, value)
		value = min(100, value)
		self.direction = -1 # -1 == left
		self.w_over_r = value
		self.set_motors()

	def straight(self):
		self.direction = 0 # 0 == straight
		self.set_motors()

	def right(self, value):
		value = max(0, value)
		value = min(100, value)
		self.direction = 1 # 1 == right
		self.w_over_r = value
		self.set_motors()

	def wait(self, value):
		time.sleep(value)

def run():
	if 'program' in sys.modules:
		del sys.modules['program']
	import program
	with Robot() as robot:
		if 'setup' in dir(program):
			program.setup(robot)
		if 'loop' in dir(program):
			while True:
				program.loop(robot)
