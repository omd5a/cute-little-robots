Program your Cute Little Robot!

# Getting Started #

## Web Page ##

You use a web browser to program and control your robot. Load the page http://omd5a.gitlab.io/cute-little-robots/webrepl-master/webrepl.html into your web browser. You need to do this while your computer is connected to the Internet, before you connect it to your robot's WiFi network.

The web page has an area where you can write your robot program, an area where you can see what's going on inside your robot (like error messages if something goes wrong), and buttons that you can use to control your robot.

## WiFi ##

You connect your computer to your Cute Little Robot with WiFi. Each robot has its own WiFi network. Look on the sticker in the bottom of your robot's battery compartment to see the last part of its WiFi network name. The whole name will be `MicroPython-` followed by what's on your robot's sticker.

Put the battery in your robot, and then connect to its network with your computer.

The robots have a password that you need to connect to their WiFi networks. The password is "micropythoN". Notice that all of the characters are lower-case, except the N at the end, which is upper case.

## Connecting to Your Robot ##

The first thing you need to do after you've connected your computer to your robot's WiFi network and loaded the web page is to connect to your robot. Notice in the upper-right of the web page there is a button labeled `Connect`. You need to click this button to start connecting to your robot.

Your robot doesn't want to let just anybody program it. It only lets people who know the password in. You'll notice when you click the `Connect` button that your robot will ask for the password in the dark area on the lower-left part of the web page. Enter the password, which is `gorobots`, and then press Enter. Your robot wants to keep the password secret, so it won't show you the password as you're typing it. You need to type carefully to enter it correctly. If you get it correct, your robot will print out `WebREPL connected` in that dark text area. If you don't get it correct, it will print out `Access denied` and disconnect from you. Just press the Connect button and try again until you get the password entered correctly.

## Programming Your Robot ##

Once you have connected to your robot and entered the password, it's time to have fun programming your robot. But first, you need to learn how to get your program into the robot, and tell your robot to start running your program, or to stop running it when you're done or when you want to change the program.

When you first connect to your robot, you'll see a very simple program in the editor part of the web page. All this program does is makes your robot print `hello` in the dark area at the bottom of the web page. Let's use this program to learn how to get started.

Notice on the right part of your web page is a red button that says `Send to robot`. This button sends your program to your robot, and it is red because you haven't sent the program that is in the editor to the robot yet. Any time you have a new program or change the program, this button turns red to remind you that you haven't sent the program in your editor to your robot yet.

Go ahead and click the `Send to robot` button. It should turn green after a second or two.

Notice that at the same time the `Send to robot` turns green, so does the `Go!` button just below it. This is the button that tells your robot to run your program. Go ahead and click it!

You should see in the dark area at the bottom of your web page that your robot says hello to you.

This program is done as soon as it says hello to you. Other programs can run until you tell them to stop, and that's what the `Stop!` button is for. If you ever have a program that runs for a long time, or forever, and you want to stop it, just click this button.

That isn't a very exciting program, but that you know how to send your program to your robot and run it, you can move on to more fun programs.

## Example Programs ##

There are a bunch of example programs that you can load into your web page to get a head start. You can send these programs to your robot and try them out, and then you can change the programs to put your own ideas into them.

To load an example program, move your mouse over the button that says `Programs...`. A menu will pop up with a bunch of differentt programs in it. If you click one of these, that program will load into your editor. Notice that the `Send to robot` and `Go!` buttons turn red when you load an example program into your editor, to remind you that you need to send it to your robot before you can tell your robot to run it.

## Setup and Loop ##

You'll notice looking at the example programs that there are two functions for you to put in your programs: `setup` and `loop`.

```python
def setup(robot):
    robot.speed(30)

def loop(robot):
    robot.forward()
    robot.wait(1)
    robot.reverse()
    robot.wait(1)
```

When you press the `Go!` button, your robot will do the `setup` function once, and then it will do the `loop` function over and over again until you press the `Stop!` button (or something goes wrong).

Notice that both `setup` and `loop` have a parameter called `robot`. What is a parameter? A parameter is how you send something into a function so the function can use it. In this case, your robot sends itself into your `setup` and `loop` functions so that your program can tell the robot what to do. Anywhere you see `robot.<something>` in the program, that's an example of the program telling the robot to do something.

## What Can Your Robot Do? ##

glow(value) -- Set the LED brightness

floor() -- Get the floor brightness

forward() -- Move forward

reverse() -- Move backward

stop() -- Stop moving

speed(value) -- Set the movement speed

left(value) -- Turn left

straight() -- Go straight

right(value) -- Turn right

wait(value) -- Wait

When you see `value` in those function descriptions, it is a parameter to tell that function how much it should do that thing. You can give it a number from 0 to 100. Small numbers mean to do it a little, and big numbers mean do it a lot. So if you say `robot.glow(100)`, you're telling the robot to make its LED bright. If you say `robet.right(10)`, you're telling the robot to turn left a little bit (because 10 is a small number, instead of a big number like 90 or 100).

## Source Code ##

You can dig deeper. The source code for this page, the web page that you use to program your robot, and source code that is running inside your robot to help your program are all online at https://gitlab.com/omd5a/cute-little-robots.
